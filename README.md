# Open Weather API

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Future enhancements
- add a bit of design flair to the UI
- move the API key to env variable
- separate pieces of UI into single-file components
- introduce Vuex as state management tool to allow communication between those single-file components
- find a better weather icon library that's available to NPM registry rather than including within Git
- include a build process for Tailwind CSS so that unused CSS isn't included

